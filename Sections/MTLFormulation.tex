\section{MTL-based House Price Prediction}\label{sect4} 
In this section, we first describe our house price prediction problem and provide preliminaries about MTL. Then we formulate the problem of MTL for house price prediction. To facilitate our illustration, the notations used throughout this paper are presented in Table~\ref{tab:sect41}.

\begin{table}[h!]
\small\centering
\begin{tabular}{| c | p{5.5cm} |}
\hline
Notations	&Explanations\\
\hline
${\pp}$, ${\PP}$	&a task (partition), all tasks (partitions)\\
${\ttt}$, ${\taut}$	&a time interval, a timestamp\\
${\hh}$	&prediction horizon\\
${\mm_{\ttt}^{\pp}}$	&number of samples for task ${\pp}$ in a time interval ${\ttt}$\\
${\mm_{\kk, \ttt}^{\pp}}$	&number of samples for task ${\pp}$ in ${\kk}$ time intervals\\
${\DD}$	&number of features\\
${\xx_{\pp}}$, ${\yy_{\pp}}$	&training input and output for task ${\pp}$\\
${\ww_{\pp}}$, ${\WW}$		&feature weight parameter for task ${\pp}$, weight matrix for all tasks\\
${\LL}, {\OmegaO}$	&empirical loss and regularization error\\
${\rr_{\pp,\qq}}$	 &Ratio of average house price for two tasks ${\pp}$ and ${\qq}$\\
\hline
\end{tabular}
\caption{Notations and explanations.}
\label{tab:sect41}
\end{table}

\subsection{Problem description and preliminaries}
Consider that the house data contains ${\PP}$ partitions and in each time interval ${\ttt}$ (e.g., month, quarter), each partition ${\pp}$ has ${\mm_{\ttt}^{\pp}}$ ${\in}$ ${\mathbb{Z}}$ house transaction records. Therefore, given a timestamp ${\taut}$, the objective of our house price prediction is to predict the price of houses that appear in each partition from ${\taut}$ to ${\taut} + {\hh}$ based on the historical transaction data collected before ${\taut}$, where ${\hh}$ is a specific prediction horizon. It is not difficult to find that multiple timestamps refer to multiple predictions.

In this paper, we comprehend each house price prediction as a problem that is jointly learned by multiple tasks. There are two key points in the formulation of such an MTL: defining tasks and characterizing the relatedness among tasks. In term of the first point, it is usually determined by the information in the data used and the specific application scenario. For example, in the widely used case of predicting student performance in schools, the 139 schools involved were defined as 139 tasks. In term of the second point, methods with regularization terms are generally used to formulate the relatedness among tasks, and different regularization terms represent different ways of formulation. For example, the $l_{2,1}$-norm means that all tasks share a common set of representations.

Here, we define the partitions contained in the house data as tasks, and each partition is aligned with a task. Thus, the two key points in MTL-based house price prediction become how to construct the ${\PP}$ tasks and what methods are used to model the relatedness among these ${\PP}$ tasks. Next, we will introduce our strategies for each of these two issues.

\subsection{Task definition}
In the literature~\cite{zhou2011multi, zhao2015multi, liu2016urban}, tasks are usually uniquely identified and given directly, thus the impact of task definitions on performance has also not received sufficient attention. However, as an essential element of using MTL, there are various ways to define tasks, even in the same application scenario. For example, in the case of predicting student performance in schools as described above, in addition to defining each school as a task, we can also group adjacent schools into one task. Obviously, this change in task definition will affect the subsequent steps of the MTL formulation. Therefore, for our house price prediction problem, we propose two categories of strategies for task definition, and explore the impact of different task definitions on prediction performance.

\subsubsection{Defining tasks based on one single profile}
Existing STL-based house price prediction approaches~\cite{bourassa2010predicting, kuntz2014geostatistical, Montero2018} can be grouped into this category, usually from the perspective of geographical factors. For example, we can define the area of a postal code as one task. One reason for this definition is that geographical factors are the most intuitive expression of house price differences, so task definition along this line is the most common one. Another reason is that the house data used by the above studies includes limited features that affect house price, making it difficult to find more ways to define tasks. Considering that our data set contains a (much richer) variety of house features, we conduct a wide range of task definition attempts and select the following four cases as representatives of this category.

In the house profile, we use the statistical area levels to split the house data. The four area levels indicate four splitting strategies. One partition in each level is defined as a task in the corresponding level. For example, there are 17 partitions at the {\SAfourID} level, so we can get 17 tasks at this level. Similarly, we also consider task definition based on postal code, where one postal code partition corresponds to a task.

In the education profile, we employ the concept of school districts to split the house data, and each school district serves as one task. The primary and secondary school districts lead to two splitting strategies. In addition, we note that the attention to the school district is closely related to the ranking of the school. Therefore, we mainly focus on the school districts of top schools.

In the transportation profile, there seems to be no obvious perspective to define the tasks compared to the previous two feature profiles. Considering that distance/time is an important criterion for measuring the situation of transportation, we define each train station as the centroid of a task and determine the scope of the task by specifying the distance/time threshold to the train station. Therefore, houses with the same train station and the distance/time to the train station (that do not exceed a pre-specified threshold)  belong to the same task.

In the facility profile, we group houses according to the similarity of the facilities. Specifically, there are four types of facilities in our data set, so we give four criteria for measuring similarity, namely {\SharedOne} (share one type), {\SharedTwo} (share two types), {\SharedThree} (share three types), and {\SharedFour} (share four types). Thus, given a criterion and the names of the facilities, such as {\SharedOne}, market, we group houses that have the same market into one task, {\SharedTwo}, shop and hospital, we group houses that have the same shop and hospital into one task.

\subsubsection{Defining tasks based on multiple profiles}
The above task definitions extract one feature profile at a time as a guideline. Such definitions not only constrain the differences in the specific feature profiles of the houses in each task, but also ensure the relatedness among tasks in terms of these profiles. However, the relatedness among tasks that depend on one feature profile are relatively weak. By introducing more feature profiles as a guideline to defining tasks, the relatedness among the resulting tasks can be strengthened, but with the refinement of the definition, the number of houses in each task may be insufficient meanwhile.

In order to guarantee sufficient number of houses necessary for each task and to enhance the relatedness between tasks, we consider six cases by combining any two of the above four task definitions, where each partition obtained corresponds to a task:  
\begin{enumerate}
	\item statistical regions and school districts;
	\item statistical regions and transportation areas; 
	\item statistical regions and neighbor facilities; 
	\item school districts and transportation areas; 
	\item school districts and neighbor facilities; 
	\item transportation areas and neighbor facilities.
\end{enumerate}

\subsection{The MTL model}
In this paper, we regard the MTL-based house price prediction problem as a multi-task regression problem. Given a timestamp ${\taut}$, we extract the transaction records in the previous ${\kk}$ time intervals to construct the training input ${\xx_{\pp}}$ ${\in}$ ${\mathbb{R}^{\mm_{\kk, \ttt}^{\pp}{\times}{\DD}}}$ and output ${\yy_{\pp}}$ ${\in}$ ${\mathbb{R}^{\mm_{\kk, \ttt}^{\pp}{\times}{1}}}$ for each task ${\pp}$. 
%
Here, ${\mm_{\kk, \ttt}^{\pp}}$ is the number of transaction records in ${\kk}$ time intervals, ${\DD}$ is the number of house features, and ${\yy_{\pp}}$ includes the actual house price. Thus, for each task, we want to infer a linear function ${f_{\pp}}$ where ${f_{\pp}(\xx_{\pp})} = {\xx_{\pp}\ww_{\pp}}$ and ${\ww_{\pp}}$ ${\in}$ ${\mathbb{R}^{\DD{\times}{1}}}$. Let us denote ${\WW = \{\ww_{1}, \ww_{2}, ..., \ww_{\PP}\}}$ ${\in}$ ${\mathbb{R}^{\DD{\times}\PP}}$ as the weight matrix over ${\PP}$ tasks. One typical MTL model for estimating $\WW$ is to minimize the following objective function:
\begin{equation}
\arg\min_{\WW} \LL(\WW) + \OmegaO(\WW)
\label{equ:sect41}
\end{equation}
where ${\LL(\WW)} = {{\sum_{\pp=1}^{\PP}}||\xx_{\pp}\ww_{\pp} - \yy_{\pp}||_{\FF}^{2}}$,  and ${\OmegaO(\WW)}$ is the regularization term that controls the common information shared among tasks.

There can be various choices of regularization terms to fit the above objective function, and the specific choice is based on the identification of the relatedness among the defined tasks. In our house price prediction problem, it is too strict or even unrealistic to use only one type of regularization term because there are various task definitions. Moreover, the purpose of this paper is to study the application of MTL for the house price prediction problem, rather than designing a sophisticated MTL-based method to fit all task definitions. Therefore, we choose three different regularization terms to model the relatedness between tasks, and thus investigate the impact of different MTL-based methods on prediction performance.

The first way is to constrain the models of all tasks to be close to each other. The $l_1$-norm (or Lasso) regularization is widely used because it can reduce model complexity and feature learning by introducing sparsity into the model, a common simplification of $l_1$-norm in MTL is that the parameter controlling the sparsity is shared among all tasks. Then the objective function can be defined as:
\begin{equation}
\arg\min_{\WW}{\sum_{\pp=1}^{\PP}}||\xx_{\pp}\ww_{\pp} - \yy_{\pp}||_{\FF}^{2} + \thetaT_{1}||\WW||_{1}
\label{equ:sect42}
\end{equation}
where ${\thetaT_{1}}$ is the parameter that controls sparsity.

The second way is to assume all tasks share a common yet latent representation, such as a common set of features, a common subspace. This motivates the group sparsity, the $l_{2,1}$-norm regularization is usually used to implement this assumption. The objective function can be expressed as:
\begin{equation}
\arg\min_{\WW}{\sum_{\pp=1}^{\PP}}||\xx_{\pp}\ww_{\pp} - \yy_{\pp}||_{\FF}^{2} + \thetaT_{1}||\WW||_{2,1}
\label{equ:sect43}
\end{equation}
where ${\thetaT_{1}}$ is the parameter controlling the group sparsity.

Besides these two most common methods, we also consider ensuring the relatedness between tasks by adding graph regularization. Specifically, the structural relatedness among ${\PP}$ tasks is represented by a graph, each task is defined as a node, and two nodes are connected by a weighted edge. The overall objective function can be described as:
\begin{equation}
\small
\arg\min_{\WW}{\sum_{\pp=1}^{\PP}}||\xx_{\pp}\ww_{\pp} - \yy_{\pp}||_{\FF}^{2} + \thetaT_{1}{\sum_{\pp,\qq=1}^{\PP}{\rr_{\pp,\qq}}||w_\pp - w_\qq||_{\FF}^{2}} + \thetaT_{2}||\WW||_{2,1}
\label{equ:sect44}
\end{equation}
where ${\rr_{\pp,\qq}}$ is the connection strength between nodes ${\pp}$ and ${\qq}$, ${\thetaT_{1}}$ and ${\thetaT_{2}}$ are parameters for graph regularization and group sparsity, respectively. 

We define ${\rr_{\pp,\qq}}$ as the ratio of the average house price for the two nodes to measure the structural relatedness between tasks ${\pp}$ and ${\qq}$. Intuitively, the larger the ${\rr_{\pp,\qq}}$, the graph regularization term will force ${\ww_{\pp}}$ to be closer to ${\ww_{\qq}}$. Meanwhile, the closer ${\ww_{\pp}}$ and ${\ww_{\qq}}$ are, the more similar the average house price for these two nodes should be, i.e., ${\rr_{\pp,\qq}}$ tends to 1. Thus, we compute ${\rr_{\pp,\qq}}$ as follows:
{\gao{
\begin{equation}
\rr_{\pp,\qq} = \frac{\min\{average price_{\pp}, average price_{\qq}\}}{\max\{average price_{\pp}, average price_{\qq}\}}
\label{equ:sect45}
\end{equation}
}}

All the optimization problems above can be solved by using the accelerated gradient descent method~\cite{ruder2016overview}. In this paper, we apply the implementation of accelerated gradient descent method included in the MALSAR~\cite{zhou2011malsar} package to efficiently solve the optimization. 




