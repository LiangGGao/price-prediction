\section{House Data Profiling}\label{sect3}
In this section, we elaborate a comprehensive real estate dataset used in this study. We first describe four location-centered profiles, each of which contain a variety of features. Then we analyze the house data to further demonstrate the motivations for introducing MTL to predict house prices. 

\subsection{Data description}
We utilize house data from Melbourne{\footnote{\url{http://www.realestate.com.au/}}}~\cite{10.1007/978-3-319-46922-5_34, LI20181}, one of the largest cities in Australia, as an example to understand the domain situation. The data includes houses sold in Melbourne's metropolitan area since 2011. We extract 136, 394 house transaction records from Jan. 2015 to Jan. 2018 to generate the dataset for this study. The house features are divided into four profiles to better observe the impact of different types of features on house price. In particular, this comprehensive dataset contains four location-centered profiles: house profile, education profile, transportation profile and facility profile. 

\noindent\textbf{House profile.} In this group, we choose seven relevant features about the house itself. The number of bedrooms, the number of bathrooms, and land size are the most common basic features. The number of parking spaces is gradually related to price with the booming house market, thus we introduce this feature. Considering the possible correlation between price and income, we also include family weekly income as an independent feature. Moreover, geographical information has a great impact on house price. In the vast majority of cases, consumers are more concerned with general locations than with detailed addresses, so we choose two regional features, {\SAID}{\footnote{\url{http://www.abs.gov.au/}}} (Statistical Area Level, the geographical areas for the processing and release of Australian census data) and postal code, to reflect the impact of geographical information. Table~\ref{tab:sect31} reports the number of partitions at different statistical area levels and postal code.
 
\begin{table}[!htbp]
\centering
\begin{tabular}{| c | c |}
\hline
Statistical area level	&\#Partitions\\
\hline
{\SAfourID}	&17\\
\hline
{\SAthreeID}	&65\\
\hline
{\SAtwoID}	&100\\
\hline
Postal code	&547\\
\hline
{\SAoneID}	&10703\\
\hline
\end{tabular}
\caption{Number of partitions at different statistical area levels. {\SAfourID} has the coarsest-grained split of an area and the smallest number of partitions, while {\SAoneID} has the finest-grained split of an area and the largest number of partitions.}
\label{tab:sect31}
\end{table} 
 
\noindent\textbf{Education profile.} In recent years, educational resources have received more and more attention, thus we either find the exact primary and secondary school districts{\footnote{\url{http://melbourneschoolzones.com/}}} (top 20\% schools) that each house belongs to, or map the house with its nearest primary and secondary schools. The corresponding school rankings{\footnote{\url{http://bettereducation.com.au/}}} as four features to examine the impact on house price.

\noindent\textbf{Transportation profile.} Since transportation networks have always been of great concern, we set up six features: (1) the distance and walking time from each house to its nearest train station using Google Maps API{\footnote{\url{http://developers.google.com/maps/}}}, (2) the distance and travel time between each pair of train stations based on the GTFS{\footnote{\url{http://www.data.vic.gov.au/data/dataset/}}} (General Transit Feed Specification) data, and (3) the distance and self-driving time from the location of the house to the city center, i.e., to the main Central Business District (CBD), using Google Maps API. 

\noindent\textbf{Facility profile.} Proximity to facilities such as shops, hospitals, clinics, and supermarkets may affect the house price as well. Therefore, we introduce four different features based upon these four facilities to describe the distance between a given house and the nearest four facilities, where the distance calculation uses Google Maps API. 

Table~\ref{tab:sect32} summarizes all selected house features and their definitions. Meanwhile, some important statistics for our data set can be found in Table~\ref{tab:sect33}. The value we want to predict, the house price at a given sales time, as the target. Figure~\ref{fig:sect31}a describes the trend of average house price in each month during the three years. It is clear that average house price is fluctuating over time, which indicates that house price is time-sensitive. Considering that location is one of the important factors in shaping the price of a house. Without loss of generality, we choose the partitions based on {\SAfourID} here. As shown in Figure~\ref{fig:sect31}b, the average house price in each partition is different. Therefore, spatial dependence also clearly exists in house price.

\begin{table*}[!htbp]
\small\centering
\begin{tabular}{| c | c | c | c |}
\hline
Category	&Name of features	&Descriptions	&Original data type\\
\hline
\multirow{7}{*}{House}
&{\BEDROOM}	&The number of bedrooms	&Ratio\\
&{\BATHROOM}	&The number of bathrooms	&Ratio\\
&{\PARKING}	&The number of parking spaces	&Ratio\\
&{\LANDSIZE}	&The land size of the house ($m^2$)	&Ratio\\ 
&{\INCOME}	&Family weekly income ($K$)	&Ratio\\
&{\SAID}	&SA1-SA4; SA1 is the finest-grained split	&Ordinal\\
&{\POSTCODE}	&Postal code	&Nominal\\
\hline
\multirow{4}{*}{Education}
&{\SCHDIST}	&The school district to which the house belongs	&Ordinal\\
&{\NEARSCH}	&The school closest to the house &Ordinal\\
&{\PRIRANK}	&The ranking of primary school	&Ordinal\\
&{\SECRANK}	&The ranking of secondary school	&Ordinal\\
\hline
\multirow{6}{*}{Transportation}
&{\DISTSTAT}	&The distance to nearest train station ($m$)	&Ratio\\
&{\TIMESTAT}	&The walking time to nearest train station ($min$)	&Ratio\\
&{\DISTCBD}	&The train distance to city center ($m$)	&Ratio\\
&{\TIMECBD}	&The train time to city center ($min$)	&Ratio\\
&{\PDISTCBD}	&The self-driving distance to city center ($m$)	&Ratio\\
&{\PTIMECBD}	&The self-driving time to city center ($min$)	&Ratio\\
\hline
\multirow{4}{*}{Facility}
&{\DISTSHOP}	&The distance to nearest shopping center	($m$)	&Ratio\\
&{\DISTHOSP}	&The distance to nearest hospital ($m$)	&Ratio\\
&{\DISTGP}	&The distance to nearest clinic ($m$)	&Ratio\\
&{\DISTMARK}	&The distance to nearest supermarket ($m$)	&Ratio\\
\hline
\end{tabular}
\caption{List of house features selected.}
\label{tab:sect32}
\end{table*}

\begin{table*}[!htbp]
\small\centering
\begin{tabular}{| c | c | c | c | c | c |}
\hline
Category	&Name of features	&Min.	&Max.	&Median	&Std. Dev.\\
\hline
\multirow{21}{*}{Features} 
&{\BEDROOM}	&1	&5	&--	&--\\
&{\BATHROOM}	&1	&3	&--	&--\\
&{\PARKING}	&1	&5	&--	&--\\
&{\LANDSIZE} ($m^2$)	&340		&2500	&708.79	&291.73\\
&{\INCOME} ($K$)	&935		&2836	&1553.91	&387.96\\
&{\SAID}	&SA1	&SA4	&--	&--\\
&{\POSTCODE}	&3000	&3996	&--	&--\\
\cline{2-6}
&{\SCHDIST}	&1	&100		&--	&--\\
&{\NEARSCH}	&1	&500		&--	&--\\
&{\PRIRANK}	&3	&500		&--	&--\\
&{\SECRANK}	&1	&500		&--	&--\\
\cline{2-6}
&{\DISTSTAT} ($m$)	&23	&5040	&2026.22	&1186.78\\
&{\TIMESTAT} ($min$)	&1	&126		&34.75	&20.36\\
&{\DISTCBD} ($m$)	&1300	&82600	&35239.52	&14594.42\\
&{\TIMECBD} ($min$)	&6	&101		&48.33	&16.64\\
&{\PDISTCBD} ($m$)	&1245	&83497	&35109.47	&14678.34\\
&{\PTIMECBD}	($min$)	&10	&120		&47.98	&18.58\\
\cline{2-6}
&{\DISTSHOP} ($m$)	&5	&4999	&1643.19	&961.48\\
&{\DISTHOSP} ($m$)	&15	&5000	&1832.83	&1102.66\\
&{\DISTGP} ($m$)	&8	&4999	&957.21	&745.01\\
&{\DISTMARK} ($m$)	&25	&5000	&1452.63	&847.71\\
\hline
\multirow{2}{*}{Targets} 
&{\DATE}	&Jan. 2015	&Jan. 2018	&--	&--\\
&{\PRICE} ($K$)	&262		&2090	&680.54	&353.97\\
\hline
\end{tabular}
\caption{Statistics of our house data.}
\label{tab:sect33}
\end{table*}

\begin{figure}[t]
\centering
\includegraphics[width=0.48\textwidth]{Figures/AvgPrice.eps}
\caption{Three-year average house price trend by Month and {\SAfourID} respectively. Average house price fluctuates over time and varies by partition.}
\label{fig:sect31}
\end{figure}

\subsection{Data insight}
Because of the time sensitivity and spatial dependence of house price as described above, the researchers~\cite{bourassa2010predicting, case2004modeling, kuntz2014geostatistical, Montero2018} intuitively split house data and model each partition individually. However, such modeling usually does not deal well with house price prediction problem. The two challenges that affect the prediction performance have been mentioned in the previous sections, and we now elaborate on them by analyzing the house data.

The first reason is that no matter which splitting strategy is adopted, it is difficult to ensure that the number of samples allocated to the generated partitions is optimal. We choose partitions based on {\SAfourID} and {\POSTCODE} as two cases, respectively. Figure~\ref{fig:sect32} shows the number of samples for each partition during the three years. We can find that the number of samples in the partitions varies greatly. Moreover, with the further refinement of the split, the number of samples in each partition is generally small, especially in areas where the number of original samples is insufficient, the impact of splitting is more pronounced, which reduces the prediction performance (please refer to the empirical results for the STL-based approaches in Tables~\ref{tab:sect52} and~\ref{tab:sect53} in the Experimental Results section).

\begin{figure}[t]
\centering
\includegraphics[width=0.48\textwidth]{Figures/NumHouses.eps}
\caption{Number of samples per partition based on {\SAfourID} and {\POSTCODE}. The number of samples in the partitions varies greatly. As the split is further refined, the number of samples is generally small.}
\label{fig:sect32}
\end{figure}

Another important reason is that independent modeling ignores the relatedness between partitions. In fact, partitions are not completely independent, and there are many explicit or implicit connections between them. For example, two independent partitions in geographical space may belong to the same school district. As a result, when analyzing the impact of the school on house price, the two partitions should be merged rather than separated. In order to better illustrate our intuition, we count the number of features that belong to multiple partitions based on {\SAfourID} and {\POSTCODE}, respectively. The results are summarized in Table~\ref{tab:sect34}. The phenomenon that features belong to multiple partitions is very common and becomes more apparent with further splitting. It also indicates the importance of preserving the relatedness between partitions for modeling the problem of house price prediction.

\begin{table}[htbp]
\small\centering
\begin{tabular}{| c | c | c | c | c | c | c |}
\hline
\multirow{2}{*}{Category}
&\multicolumn{3}{c |}{\SAfourID}	&\multicolumn{3}{c |}{\POSTCODE}\\
\cline{2-7}
&=1	&=2	&$\geq$3	&=1	&=2	&$\geq$3\\
\hline
PRIMARY	&\multirow{2}{*}{46}	&\multirow{2}{*}{50}	&\multirow{2}{*}{225}		&\multirow{2}{*}{34}	&\multirow{2}{*}{30}	&\multirow{2}{*}{257}	\\
\_SCHOOL	&&&&&&\\
SECONDARY	&\multirow{2}{*}{40}	&\multirow{2}{*}{32}	&\multirow{2}{*}{148}		&\multirow{2}{*}{24}	&\multirow{2}{*}{23}	&\multirow{2}{*}{173}	\\
\_SCHOOL	&&&&&&\\
\hline
{\STATION}	&123		&70	&25	&11	&33	&174	\\
\hline
{\SHOP}	&289		&64	&17	&84	&84	&202	\\
{\HOSPITAL}	&328		&83	&13	&94	&87		&243	\\
{\GP}	&2133	&112	&7	&1386	&633		&233	\\
{\MARKET}	&361	&62	&12	&65	&101		&269	\\
\hline
\end{tabular}
\caption{Number of features that belong to only one partition (=1), two partitions (=2), and at least three partitions (${\geq}$3) based on {\SAfourID} and {\POSTCODE}.}
\label{tab:sect34}
\end{table}

Based on the above data analysis, we conclude the two challenges that affect the prediction performance of existing approaches from the data view. Meanwhile, considering that the inherent relatedness of the partitions due to the consistency of the house features, we cast the house price prediction problem into an MTL problem. By using the framework of MTL, we can not only reflect the relatedness between partitions well, but also solve the dilemma of insufficient samples in some partitions.




