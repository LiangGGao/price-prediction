\documentclass[10pt,journal,twoside,compsoc]{IEEEtran}

\usepackage{graphicx}
\usepackage{txfonts}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{array}
\usepackage{subfig}
\usepackage[usenames, dvipsnames]{xcolor}
\usepackage[nocompress]{cite}
\usepackage[colorlinks, linkcolor=black, urlcolor = black, citecolor=black]{hyperref}
\usepackage{paperSyml}
\usepackage{pifont}
\usepackage{paperSyml}

\newcommand{\YES}{\color{ForestGreen}{\ding{52}}}
\newcommand{\NO}{\color{Red}{\ding{56}}}

\newcommand{\bao}[1]{\textrm{\textcolor{red}{#1}}}
\newcommand{\gao}[1]{\textrm{\textcolor{orange}{#1}}}

\begin{document}

\title{Location-Centered House Price Prediction: A Multi-Task Learning Approach}

%\begin{comment}
\author{
Guangliang~Gao,
\IEEEcompsocitemizethanks{
	\IEEEcompsocthanksitem G. Gao is with the School of Computer Science and Engineering, Nanjing University of Science and Technology, Nanjing, China.\protect\\
	E-mail: guangliang.gao@njust.edu.cn}% <-this % stops an unwanted space
Zhifeng~Bao,
\IEEEcompsocitemizethanks{
	\IEEEcompsocthanksitem Z. Bao is with the School of Science, Computer Science, and Information Technology, RMIT University, Melbourne, VIC 3000, Australia.\protect\\
	E-mail: zhifeng.bao@rmit.edu.au}% <-this % stops an unwanted space
Jie~Cao,
\IEEEcompsocitemizethanks{
	\IEEEcompsocthanksitem J. Cao is with the School of Information Engineering, Nanjing University of Finance and Economics, Nanjing, China, and with the College of Computer Science and Engineering, Nanjing University of Science and Technology, Nanjing, China.\protect\\
	E-mail: caojie690929@163.com}% <-this % stops an unwanted space
A. K.~Qin,
Timos~Sellis,~\IEEEmembership{Fellow,~IEEE,}
\IEEEcompsocitemizethanks{
	\IEEEcompsocthanksitem A. K. Qin and T. Sellis are with the Data Science Research Institute, Swinburne University of Technology, Melbourne, VIC 3122, Australia.\protect\\
	E-mail: \{kqin, tsellis\}@swin.edu.au.}% <-this % stops an unwanted space	
%Zhiang~Wu,~\IEEEmembership{Member,~IEEE}
Zhiang~Wu
\IEEEcompsocitemizethanks{
	\IEEEcompsocthanksitem Z. Wu (corresponding author) is with the School of Information Engineering, Nanjing University of Finance and Economics, Nanjing, China.\protect\\
	E-mail: zawuster@gmail.com}% <-this % stops an unwanted space
}
%\end{comment}

\IEEEtitleabstractindextext{%
\begin{abstract}
Accurate house prediction is of great significance to various real estate stakeholders such as house owners, buyers, investors, and agents. We propose a location-centered prediction framework that differs from existing work in terms of data profiling and prediction model. Regarding data profiling, we make an important observation as follows -- besides the in-house features such as floor area, the location plays a critical role in house price prediction. Unfortunately, existing work either overlooked it or had a coarse grained measurement of locations. Thereby, we define and capture a fine-grained location profile powered by a diverse range of location data sources, such as transportation profile (e.g., distance to nearest train station, door-to-door travel time by public transportation), education profile (e.g., school zones and ranking), suburb profile based on census data, facility profile (e.g., nearby GPs, hospitals, supermarkets). 
%
Regarding the choice of prediction model, we observe that a variety of approaches either consider the entire data for modeling, or split the entire house data and model each partition independently. However, such modeling ignores the relatedness between partitions, and for all prediction scenarios, there may not be sufficient training samples per partition for the latter approach. We address this problem by conducting a careful study of exploiting the Multi-Task Learning (MTL) model. Specifically, we map the strategies for splitting the entire house data to the ways the tasks are defined in MTL, and each partition obtained is aligned with a task. Furthermore, we select specific MTL-based methods with different regularization terms to capture and exploit the relatedness between tasks. Based on real-world house transaction data collected in Melbourne, Australia from Jan. 2015 to Jan. 2018, these transactions are recorded daily for three years, including house sales price and various house features. We design extensive experimental evaluations, and the results indicate a significant superiority of MTL-based methods over state-of-the-art approaches. Meanwhile, we conduct an in-depth analysis on the impact of task definitions and method selections in MTL on the prediction performance, and demonstrate that the impact of task definitions on prediction performance far exceeds that of method selections.
\end{abstract}

\begin{IEEEkeywords}
Price prediction, real estate, multi-task learning
\end{IEEEkeywords}
}
% make the title area
\maketitle

% main body
\input{Sections/Introduction.tex}
\input{Sections/HousePricePrediction.tex}
\input{Sections/HouseDataProfiling.tex}
\input{Sections/MTLFormulation.tex}
\input{Sections/ExperimentResults.tex}
\input{Sections/Conclusion.tex}

\section*{Acknowledgments}
This work was partially supported by ARC under Grants DP170102726, DP180102050, DP170102231, and the National Natural Science Foundation of China (NSFC) under Grants 61728204, 91646204, and 71571093. Zhifeng Bao is a recipient of Google Faculty Award.

% references section
\bibliographystyle{IEEEtran}
\bibliography{paperRefs}

%\begin{comment}

% biography section
\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Authors/GuangliangGao}}]
{Guangliang Gao} received the B.E. degree from the Anhui University of Science and Technology, Anhui, China, in 2012. He is currently pursuing the Ph.D. degree with the Nanjing University of Science and Technology, Nanjing, China. He is currently a visiting student at Computer Science and Information Technology, RMIT University, Melbourne, VIC, Australia. His current research interests include data mining and machine learning over social network data and geo-spatial data.
\end{IEEEbiography}\vspace{-1.2cm}

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Authors/ZhifengBao}}]
{Zhifeng Bao} received the Ph.D. degree in computer science from the National University of Singapore in 2011 as the winner of the Best PhD Thesis in school of computing. He is currently a senior lecturer with the RMIT University and leads the big data research group at RMIT. He is also an Honorary Fellow with University of Melbourne in Australia. His current research interests include data usability, spatial database, data integration, and data visualization.
\end{IEEEbiography}\vspace{-1.2cm}

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Authors/JieCao}}]
{Jie Cao} received his Ph.D. degree from Southeast University, China, in 2002. He is currently a chief professor and the dean of School of Information Engineering at Nanjing University of Finance and Economics. He is also a Ph.D. advisor of Nanjing University of Science and Technology. His main research interests include data mining, big data and e-commerce intelligence. He is the member of the ACM, CCF and IEEE Computer Society.
\end{IEEEbiography}\vspace{-1.2cm}

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Authors/KaiQin}}]
{A. K. Qin} received the Ph.D. degree from Nanyang Technology University, Singapore, in 2007. Since 2013, he has been the Vice-Chancellor's Research Fellow, a Lecturer, and a Senior Lecturer with RMIT University, Melbourne, VIC, Australia. In 2017, he joined Swinburne University of Technology, Melbourne, VIC, Australia, as an Associate Professor. He is now leading Swinburne's Intelligent Data Analytics Lab as well as Machine Learning and Intelligent Optimization (MLIO) Research Group. His major research interests include evolutionary computation, machine learning, computer vision, GPU computing, and services computing.
\end{IEEEbiography}\vspace{-1.2cm}

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{Authors/TimosSellis}}]
{Timos Sellis} received the Ph.D. degree in computer science from the University of California, Berkeley, in 1986. He is a professor and director of the Data Science Research Institute at Swinburne University of Technology, Australia. Between 2013 and 2015, he was a professor at RMIT University, Australia, and before 2013 the director of the Institute for the Management of Information Systems (IMIS) and a professor at the National Technical University of Athens, Greece. His research interests include big data, data streams, personalization, data integration, and spatio-temporal database systems. He is a fellow of the IEEE and ACM. In 2018 he was awarded the IEEE TCDE Impact Award, in recognition of his impact in the field and for contributions to database systems research and broadening the reach of data engineering research.
\end{IEEEbiography}\vspace{-1.2cm}

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio] {Authors/ZhiangWu}}]
{Zhiang Wu} (S'14-M'18) received his Ph.D. degree in Computer Science from Southeast University, China, in 2009. He is currently a full professor of School of Information Engineering at Nanjing University of Finance and Economics. His recent research focuses on distributed computing, data mining, e-commerce intelligence and social network analysis. He is the member of the ACM, IEEE and a senior member of CCF.
\end{IEEEbiography}

%\end{comment}

\end{document}


